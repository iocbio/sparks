# Semi-automatic calcium sparks detection software

IOCBIO Sparks is a cross-platform application for analysis and
detection of calcium sparks when imaged using confocal microscope line
scanning mode.

IOCBIO Sparks is a free software released under the GNU General Public
License (GPL), see the file [`LICENSE`](LICENSE) for details.

See [IOCBIO Sparks homepage](https://iocbio.gitlab.io/sparks) for
brief description, links and installation instructions.

## Citations and software description

Software is described in a paper published in PeerJ (see below) that
gives a background information regarding use of the software and shows
an example analysis of sparks. Please cite this paper if you use the
software.

Laasmaa, M., Karro, N., Birkedal, R., & Vendelin, M. (2019). IOCBIO
Sparks detection and analysis software. _PeerJ_, 7, e6652
https://doi.org/10.7717/peerj.6652

## Releases

All releases are listed at
[Releases](https://gitlab.com/iocbio/sparks/-/releases). Releases are
distributed as executable (for Windows) and through The Python Package
Index (PyPI). 

## Copyright

Copyright (C) 2018 Laboratory of Systems Biology, Department of
Cybernetics, School of Science, Tallinn University of Technology.

Authors:
* Martin Laasmaa <martin@sysbio.ioc.ee>
* Marko Vendelin <markov@sysbio.ioc.ee>

Software license: the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version. See [`LICENSE`](LICENSE) for details
