## Making packages

### Adjust version number

```
emacs setup.py

git status
git add setup.py
git commit -m "bump version"
git push origin master
```

### Update resources

If icons or splash screen have been changed, update resources by running

```
pyrcc5 resources.qrc -o iocbio/sparks/gui/resources.py
```

### Creating requirements file for automatic installation

To create `requirements.txt` first install Sparks software in a new
virtual environment and make sure that the software works as it
should.  Then use `pip` to create `requirements.txt`:

```
python -m venv install-test
install-test/bin/pip install .
install-test/bin/pip freeze | sort | grep -v iocbio > requirements.txt
echo iocbio.db >> requirements.txt
```

After that, `requirements.txt` should not contain `iocbio.sparks` and have
`iocbio.db` without version. Update the file in the repository.

### Formatting

Formatting is checked by `black` and `flake8`. For flake8, don't forget to install
`flake8-class-attributes-order`.

### PyPi

For making source package, run

```
python3 setup.py sdist
```

For upload to PyPI:

```
twine upload dist/iocbio.sparks-1.0.0.tar.gz
```

### Make release at Gitlab

Go to https://gitlab.com/iocbio/sparks/tags and make a new release
with Changelog
