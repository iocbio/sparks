# -*- mode: python ; coding: utf-8 -*-

block_cipher = None
import os


a = Analysis([os.path.join(os.getcwd(),'gui.py')],#script
             pathex=[], #Where to put the bundled app (default: ./dist)
             datas=[],
             binaries=[],
             hiddenimports=[],# Name an import not visible in the code of the script(s).
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='gui', # app name
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               upx_exclude=[],
               name='app') # Folder name where all needed is stored
