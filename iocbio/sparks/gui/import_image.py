# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2018
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  Authors: Martin Laasmaa and Marko Vendelin
#  This file is part of project: IOCBIO Sparks
#

from PySide6.QtCore import Qt, QByteArray, QSettings
from PySide6.QtWidgets import QWidget, QGridLayout, QVBoxLayout, QHBoxLayout, QLabel, QPushButton, QComboBox
from PySide6.QtGui import QPalette, QTransform
import pyqtgraph as pg

from iocbio.sparks.constants import application_name
from iocbio.sparks.io.image import RawImage
from iocbio.sparks.io.hash import calcid
from iocbio.sparks.gui.custom_widgets import CustomQLineEdit, SmallLabel, EmptyContextMenuViewBox
from iocbio.sparks.handler.experiment import ExperimentHandler


class ChannelSelector(QWidget):
    def __init__(self) -> None:
        QWidget.__init__(self)

        main_layout = QVBoxLayout()
        layout = QGridLayout()
        layout.setAlignment(Qt.AlignTop)
        layout.addWidget(QLabel("Pixel size X:"), 0, 0)
        main_layout.addLayout(layout)
        self.show()


class ImportImage(QWidget, RawImage):
    """Class for importing tiff files"""

    settingGeometry = "Import Tiff image GUI/geometry"

    def __init__(self, filename, database):
        QWidget.__init__(self)
        RawImage.__init__(self, filename, database)

        self.database = database
        self.ready = False
        self.transposed = 0

        experiment_id = None
        if not self.has_channels or self.channel is not None:
            experiment_id = self.get_experiment_id()

        psql = "filename, duration, length, pixels_time, pixels_space, transposed"
        record = None
        if ExperimentHandler.has_record(database, experiment_id):
            record = self.database.query_first(
                "SELECT %s FROM " % psql
                + self.database.table(ExperimentHandler.database_table)
                + " WHERE experiment_id=:eid",
                eid=experiment_id,
            )

        self.new_record = True
        if record is not None:
            self.new_record = False
            self.ready = True
            self.dx = record.length / record.pixels_space * 1e-6
            self.dt = record.duration / record.pixels_time * 1e-3
            self.transposed = record.transposed
            return

        if not self.has_channels and self.data_dtype != "tif":
            self.ready = True
            return

        self.dx = 0.1e-6 if self.dx is None else self.dx  # meters
        self.dt = 1.0e-3 if self.dt is None else self.dt  # seconds

        main_layout = QVBoxLayout()

        layout = QGridLayout()
        layout.setVerticalSpacing(1)
        layout.setAlignment(Qt.AlignTop)

        self.pixel_space = CustomQLineEdit(self.dx * 1e6, float, "space")
        self.pixel_time = CustomQLineEdit(self.dt * 1e3, float, "time")
        transpose_image_btn = QPushButton("Flip dimensions")
        channel_selector = QComboBox()
        channel_selector.addItems([str(i) for i in range(self.number_of_channels)])
        start_btn = QPushButton("Start analysis")
        btn_layout = QHBoxLayout()

        # { Connecting events
        self.pixel_space.sigValueUpdated.connect(self.update_resolution)
        self.pixel_time.sigValueUpdated.connect(self.update_resolution)
        transpose_image_btn.clicked.connect(self.transpose_image)
        channel_selector.currentIndexChanged.connect(self.update_channel)
        start_btn.clicked.connect(self.start)
        # }

        layout.addWidget(QLabel("Pixel size X:"), 0, 0)
        layout.addWidget(self.pixel_space, 0, 1)
        layout.addWidget(SmallLabel("Specify space resolution in micrometers"), 1, 0, 1, 2)
        layout.addWidget(SmallLabel(""), 2, 0, 1, 2)
        layout.addWidget(QLabel("Pixel size T:"), 3, 0)
        layout.addWidget(self.pixel_time, 3, 1)
        layout.addWidget(SmallLabel("Specify time resolution in milliseconds"), 4, 0, 1, 2)
        layout.addWidget(SmallLabel(""), 5, 0, 1, 2)
        layout.addWidget(QLabel("Select channel:"), 6, 0)
        layout.addWidget(channel_selector, 6, 1, 1, 1)
        layout.addWidget(SmallLabel("Select a channel to analyze"), 7, 0, 1, 2)
        layout.addWidget(SmallLabel(""), 8, 0, 1, 2)

        btn_layout.addWidget(transpose_image_btn)
        btn_layout.addWidget(start_btn)

        # { Setting graphics general properites
        pg.setConfigOption("antialias", True)
        pg.setConfigOption("background", self.palette().color(QPalette.Window))
        pg.setConfigOption("foreground", "#000000")
        # }

        gw = pg.GraphicsLayoutWidget()
        self.plot_item = gw.addPlot(viewBox=EmptyContextMenuViewBox())
        self.image_item = pg.ImageItem(autoDownsample=True)
        self.plot_item.addItem(self.image_item)
        self.plot_item.setLabel("left", "x", units="m")
        self.plot_item.setLabel("bottom", "t", units="s")
        self.update_channel(channel_selector.currentIndex())
        self.update_image(channel_selector.currentIndex())

        layout_widget = QWidget()
        layout_widget.setLayout(layout)

        main_layout.addWidget(layout_widget)
        main_layout.addLayout(btn_layout)
        main_layout.addWidget(gw)

        self.setWindowTitle(f"{application_name} : importing file - {filename}")
        self.setLayout(main_layout)

        # load settings
        settings = QSettings()
        self.restoreGeometry(settings.value(ImportImage.settingGeometry, QByteArray()))

    def get_experiment_id(self):
        if self.channel is None:
            return calcid(arr=self.image)

        return calcid(arr=self.image[self.channel])

    def update_channel(self, channel):
        self.channel = int(channel)
        self.update_image(self.channel)

    def get_2dimage(self, channel=None):
        image = self.image[:, channel, :] if self.has_channels else self.image
        if self.transposed:
            image = image.T
        return image

    def get_data(self):
        image = self.get_2dimage(channel=self.channel)
        self.dimTime, self.dimX = image.shape
        return image, self.dx, self.dt, self.dimX, self.dimTime, self.transposed, self.get_experiment_id()

    def transpose_image(self):
        self.transposed = abs(self.transposed - 1)
        self.update_image(self.channel)

    def update_resolution(self, dimention):
        self.dx = self.pixel_space.get_value * 1e-6
        self.dt = self.pixel_time.get_value * 1e-3
        self.update_image(self.channel)

    def update_image(self, channel):
        image = self.get_2dimage(channel=channel)
        tr = QTransform()
        tr.scale(self.dt, self.dx)
        self.image_item.setTransform(tr)
        self.image_item.setImage(image)
        self.set_range(image)

    def set_range(self, image):
        t1, x1 = image.shape
        self.plot_item.setRange(xRange=[0, self.dt * t1], yRange=[0, self.dx * x1], padding=0)

    def start(self):
        self.ready = True
        self.close()

    def closeEvent(self, event):
        settings = QSettings()
        settings.setValue(ImportImage.settingGeometry, self.saveGeometry())
        self.close()
        event.accept()
