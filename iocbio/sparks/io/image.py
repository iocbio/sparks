# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#
#  Copyright (C) 2018
#   Laboratory of Systems Biology, Department of Cybernetics,
#   School of Science, Tallinn University of Technology
#  Authors: Martin Laasmaa and Marko Vendelin
#  This file is part of project: IOCBIO Sparks
#

import numpy as np
import xml.etree.ElementTree as ET

from pathlib import Path
from tifffile.tifffile import TiffFile
from czifile import CziFile


class RawImage:

    def __init__(self, filename, database):
        self._filename, self.channel = RawImage._process_filename(filename)
        self.database = database
        self.file_type = self._filename.suffix
        self.dx = None
        self.dt = None
        self.dimX = None
        self.dimTime = None
        self.transposed = 0
        self.data_dtype = None
        self.axes = None

        if self.file_type == ".czi":
            self._load_czi_file()
        elif self.file_type == ".lsm":
            self._load_lsm_file()
        elif self.file_type == ".tif" or self.file_type == ".tiff":
            self._load_tif_file()
        else:
            raise NotImplementedError(f"Such file type as {self.file_type} is not yet supported!")

    @property
    def filename(self):
        if self.channel is None:
            return str(self._filename)
        else:
            return str(self._filename) + ":channel-" + str(self.channel)

    @property
    def shape(self):
        if self.image is None:
            return None
        return self.image.shape

    @property
    def has_channels(self):
        if "C" in self.axes:
            return True
        return False

    @property
    def number_of_channels(self):
        if self.has_channels:
            return self.image.shape[self.axes.index("C")]
        return 1

    def _load_czi_file(self):
        self.data_dtype = "czi"
        with CziFile(self._filename) as czi:
            _axes = czi.axes
            tmp = czi.asarray()
            self.axes = [ax for i, ax in enumerate(_axes) if tmp.shape[i] > 1]
            self.image = np.squeeze(tmp).astype(float)

            metadata = czi.metadata()
            root = ET.fromstring(metadata)

            for d in root.findall("./Metadata/Scaling/Items/Distance"):
                el = d.attrib["Id"]
                if el in _axes:
                    setattr(self, f"d{el.lower()}", float(d[0].text))

            self.dt = float(
                root.findall("./Metadata/Information/Image/Dimensions/T/Positions/Interval/Increment")[0].text
            )
            self.dimX = int(list(root.iter("SizeX"))[0].text)
            self.dimTime = int(list(root.iter("SizeT"))[0].text)

    def _load_lsm_file(self):
        self.axes = ["T", "X"]
        self.data_dtype = "lsm"
        with TiffFile(self._filename) as tif:
            if tif.is_lsm is True:
                _image = tif.asarray()
                self.image = np.squeeze(_image)
                if _image.ndim == 3:
                    self.axes = ["T", "C", "X"]
                    self.image = np.swapaxes(self.image, 0, 1)

                self.dx = tif.lsm_metadata["VoxelSizeX"]  # in meters
                self.dt = tif.lsm_metadata["TimeIntervall"]  # in seconds
                self.dimX = tif.lsm_metadata["DimensionX"]
                self.dimTime = tif.lsm_metadata["DimensionTime"]

    def _load_tif_file(self):
        self.axes = ["T", "X"]
        self.data_dtype = "tif"
        with TiffFile(self._filename) as tif:
            _image = tif.asarray()
            self.image = np.squeeze(_image)
            if self.image.ndim == 3:
                self.axes = ["T", "C", "X"]
                self.image = np.swapaxes(self.image, 0, 1)

    @staticmethod
    def _process_filename(filename):
        parts = filename.rsplit(":channel-", maxsplit=1)
        len_parts = len(parts)
        if len_parts == 1:
            return Path(parts[0]), None
        else:
            return Path(parts[0]), int(parts[1])
