# Semi-automatic calcium sparks detection software

IOCBIO Sparks is a cross-platform application for analysis and
detection of calcium sparks when imaged using confocal microscope line
scanning mode.

<img src="img/screenshot.png">

Results of the analysis are stored in the database (schema described
in [database-schema](database-schema) and, if needed, can be exported
into spreadsheet file.

## Links

- [Project page](https://gitlab.com/iocbio/sparks)
- [Installation instructions](install.md)
- [Releases](https://gitlab.com/iocbio/sparks/-/releases)
- [Issues](https://gitlab.com/iocbio/sparks/issues)
- [Demo](https://www.youtube.com/watch?v=8xWKB--RNn4)
- [Example datasets are on SysBio homepage](https://sysbio.ioc.ee/software/iocbio-sparks/)

## Citations and software description

Software is described in a paper published in PeerJ (see below) that
gives a background information regarding use of the software and shows
an example analysis of sparks. Please cite this paper if you use the
software.

Laasmaa, M., Karro, N., Birkedal, R., & Vendelin, M. (2019). IOCBIO
Sparks detection and analysis software. _PeerJ_, 7, e6652
https://doi.org/10.7717/peerj.6652

## Copyright

Copyright (C) 2018-2021 Laboratory of Systems Biology, Department of
Cybernetics, School of Science, Tallinn University of Technology.

Authors and contacts:

* Martin Laasmaa <martin@sysbio.ioc.ee>
* Marko Vendelin <markov@sysbio.ioc.ee>

Software license: the GNU General Public License as published by the
Free Software Foundation, either version 3 of the License, or (at your
option) any later version.
